let html2 = "";
let data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];

const euro = new Intl.NumberFormat('fr-FR', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 2
});

function baseTomate(element){
    return element.base.localeCompare('tomate')==0;
}
function inf6(element){
    return element.price_small<6;
}
function occI(element){
    let tmp = 0;
    for(let i = 0; i < element.name.length; i++){
        if(element.name.charAt(i)=='i'){
            tmp +=1;
        }
    }
    return tmp >= 2;
}

data.sort(function(a, b) {
    //var textA = a.name.toUpperCase();
    //var textB = b.name.toUpperCase();
    var textA = a.price_small;
    var textB = b.price_small;
    if(textA-textB==0){
        textA=a.price_large;
        textB=b.price_large;
    }
    return (textA-textB);
});

console.log(data);
//data = data.filter(occI);
data.forEach(ShowImage);
function ShowImage(element){
    const {name,image,price_small,price_large} = element;
    let psmall = euro.format(price_small);
    let plarge = euro.format(price_large);
    const url = 'images/'+name.toLowerCase()+'.jpg';
    const html = '<article class="pizzaThumbnail">\n' +
                    '<a href="'+image+'">' +
                        '<img src="'+image+'"/>' +
                            '<section>' +
                                '<h4>'+name+'</h4>' +
                                '<ul>' +
                                    '<li>Prix petit format : '+psmall+'</li>'+
                                    '<li>Prix grand format : '+plarge+'</li>'+
                                '</ul>'+
                            '</section>'+
                    '</a>'+
                  '</article>';                  
    console.log(url);
    console.log(html);
    html2 += html;
}
document.querySelector('.pageContent').innerHTML = html2;
